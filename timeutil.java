/**
         * 格式化用户上次登录时间
         * @param lastLoginTime
         * @return 
         * */
        private String getLastLoginStr(Date lastLoginTime){
                GregorianCalendar cal = new GregorianCalendar();
                Date current=new Date();
                int index=7;
                cal.setTime(lastLoginTime);
                cal.add(Calendar.DATE, 7);//调整到上次登录7天后时间戳
                //如果上次登录时间+7还小于当前时间不执行直接return 7天前登录
                while(current.getTime()<cal.getTime().getTime()){
                        //时间戳回退一天
                        index--;
                        cal.add(Calendar.DATE, -1);
                        if(index==0){
                                break;
                        }
                        if(current.getTime()>cal.getTime().getTime()){
                                break;
                        }
                }
                if(index==0){
                        return "24小时内登录过";
                }else if(index==7){
                        return "7天前登录";
                }else{
                        return index+"天前登录";
                }
        }